// Теоретичне питання

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Конструкцію try...catch доречно використовувати для того, щоб "відловити" можливі помилки та обійти їх, не
// зламавши коду. Але тут варто зазначити, що це не ситнаксичні помилки у коді, а логічні, або такі, що виникають
// у разі некоректних даних.

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];


class InformationError extends Error {
    constructor(data) {
        super();
        this.name = "InformationError";
        this.message = `Not enough information about ${data} of book`;
    }
}

class Book {
    constructor(book) {
        if (!("author" in book)) {
            throw new InformationError("author")
        } else if (!("name" in book)) {
            throw new InformationError("name")
        } else if (!("price" in book)) {
            throw new InformationError("price")
        }
        this.author = book.author;
        this.name = book.name;
        this.price = book.price;
    }

    render(list) {
        list.insertAdjacentHTML("beforeend", `
        <li>Книга "${this.name}", автор ${this.author}, ціна ${this.price} грн</li>`)
    }

}

function createBooksList(array) {
    let booksList = document.createElement("ul");
    document.querySelector("#root").prepend(booksList);
    array.forEach(el => {
        try {
            new Book(el).render(booksList);
        } catch (err) {
            if (err.name === "InformationError") {
                console.warn(err);
            } else {
                throw err;
            }
        }
    })
}

createBooksList(books)


